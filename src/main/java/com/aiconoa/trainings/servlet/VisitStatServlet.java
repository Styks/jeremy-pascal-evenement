package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Stats;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HttpErrorHandler;

@WebServlet("/VisitStatServlet")
public class VisitStatServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Inject
    private EventService eventService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder chartRowBuilder = new StringBuilder();
        HttpSession httpSession = request.getSession();
        if (httpSession.getAttribute("username") == null) {
            try {
                response.sendRedirect("LoginServlet");
            } catch (IOException | IllegalStateException e) {
                HttpErrorHandler.print500(response, e, "Probleme redirection");
                return;
            }
        }
        String sessionUsername = httpSession.getAttribute("username").toString();
        if (sessionUsername.compareTo("admin") != 0) {
            HttpErrorHandler.print401(response, "L'autheur n'a pas accès a cette page");
            return;
        }
        try {
            List<Stats> statsList = eventService.nbrVisitByURL();
            for (Stats stats : statsList) {
                chartRowBuilder.append(", ['" + stats.getWebpageName() + "', " + stats.getCount() + "]");
            }
            String chartRow = "['', 0]" + chartRowBuilder.toString();
            request.setAttribute("statsList", statsList);
            request.setAttribute("chartRow", chartRow);
            request.getRequestDispatcher("/WEB-INF/visitStat.jsp")
                   .forward(request, response);

        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (ServletException | IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "forward impossible");
            return;
        }
    }
}
