package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.EventStatData;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HttpErrorHandler;
import com.aiconoa.trainings.services.LoginService;

@WebServlet("/EventStatServlet")
public class EventStatServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    private static final int ACCEPT = 1;
    private static final int DECLINE = 2;
    private static final int PRESENT = 1;
    private static final int NOTPRESENT = 2;
    @Inject
    private EventService eventService;
    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Author author;
        HttpSession httpSession = request.getSession();
        if (httpSession.getAttribute("username") == null) {
            try {
                response.sendRedirect("LoginServlet");
            } catch (Exception e) {
                LOGGER.error("redirection impossible", e);
           }
        }
        String sessionUsername = httpSession.getAttribute("username").toString();

        try {
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!eventService.checkIfEventCorrespondToAuthor(idEvent, sessionUsername, response)) {
                return;
            }

            EventStatData eventStatData = new EventStatData();
            eventStatData.setIdEvent(idEvent);

            // recherche de l'evenement
            Event event = eventService.selectEventByIdEvent(idEvent);

            // recherche de l'auteur
            author = loginService.selectIdAuthorByUsername(sessionUsername);

            // recherche des membres ajoutés et statuts
            List<Subscriber> subscriberList = eventService.listSelectSubscribersByIdEvent(idEvent);
            eventStatData.setNbTotalInvitation(subscriberList.size());

            // recherche du nombre de participants ACCEPT
            eventStatData.setNbIsPresent(eventService.countNumberPresentOrNot(idEvent, ACCEPT));

            // recherche du nombre de participants DECLINE
            eventStatData.setNbIsNotPresent(eventService.countNumberPresentOrNot(idEvent, DECLINE));

            // recherche du nombre de mails lus
            eventStatData.setNbIsRead(eventService.countNumberMailRead(idEvent));

            // recherche du nombre de presents apres ouverture du mail
            eventStatData.setNbIsPresentAfterReading(
                    eventService.countNbIsPresentOrNotAfterReading(idEvent, PRESENT));

            // recherche du nombre d'absents apres ouverture du mail
            eventStatData.setNbIsAbsentAfterReading(
                    eventService.countNbIsPresentOrNotAfterReading(idEvent, NOTPRESENT));

            // passage des arguments a la jsp
            request.setAttribute("subscriberList", subscriberList);
            request.setAttribute("eventStatData", eventStatData);
            request.setAttribute("title", event.getTitle());
            request.setAttribute("author", author);
            request.getRequestDispatcher("/WEB-INF/eventStat.jsp")
                   .forward(request, response);

        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (NumberFormatException e) {
            HttpErrorHandler.print404(response, e, "Le parametre de la requete http ne peut pas etre converti en Integer dans le doGet de EventStatServlet");
            return;
        } catch (ServletException | IOException | IllegalStateException exception) {
            HttpErrorHandler.print500(response, exception, "forward impossible");
            return;
        }
    }
}
