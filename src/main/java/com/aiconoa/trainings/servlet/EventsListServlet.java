package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HttpErrorHandler;
import com.aiconoa.trainings.services.LoginService;

@WebServlet("/EventsListServlet")
public class EventsListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger();
    @Inject
    private EventService eventService;
    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        if (httpSession.getAttribute("username") == null) {
            try {
                response.sendRedirect("LoginServlet");
            } catch (Exception e) {
                LOGGER.error("redirection impossible", e);
            }
        }
        String sessionUsername = httpSession.getAttribute("username").toString();
        Author author;
        
        try {
            
            List<Event> events = eventService.listSelectEventColumns(sessionUsername);
            author = loginService.selectIdAuthorByUsername(sessionUsername);
            request.setAttribute("author", author);
            request.setAttribute("events", events);
            request.getRequestDispatcher("/WEB-INF/eventsList.jsp").forward(request, response);
        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (Exception e) {
            LOGGER.info("probleme avec le forward du dispatcher", e);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.sendRedirect("EventCreationServlet");
        } catch (Exception e) {
            LOGGER.info("probleme avec la redirection dans le doPost de EventsListServlet", e);
        }
    }
}
