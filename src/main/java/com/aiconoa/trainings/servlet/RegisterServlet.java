package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.mindrot.jbcrypt.BCrypt;

import com.aiconoa.trainings.services.LoginService;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger();
    private static final String ERROR = "Error";
    private static final String USERNAME = "username";
    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // presenter un formulaire de connection
        String error = request.getParameter(ERROR);
        if (error != null) {
            request.setAttribute(ERROR, ERROR);
        }
        try {
            request.getRequestDispatcher("/WEB-INF/register.jsp")
                    .forward(request, response);
        } catch (Exception e) {
            LOGGER.error("forward impossible", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter(USERNAME);
        HttpSession httpSession = request.getSession();
        try {
            if (httpSession.getAttribute(USERNAME) != username) {
                response.getWriter().append("username was set in the session \n");
                httpSession.setAttribute(USERNAME, username);
            }
            if (request.getParameter("password1").compareTo(request.getParameter("password2")) != 0) {
                response.sendRedirect("RegisterServlet?Error=1");
                return;
            }
            String password = BCrypt.hashpw(request.getParameter("password1"), BCrypt.gensalt(12));
            if (loginService.checkIfAuthorExist(username)) {
                response.sendRedirect("RegisterServlet");
                return;
            }
            loginService.insertAuthor(username, password);
            request.setAttribute("flash.message", "Vous vous êtes enregistré avec succès");
            response.sendRedirect("EventCreationServlet");
        } catch (Exception e) {
            LOGGER.error("probleme avec la reponse", e);
        }
    }
}
