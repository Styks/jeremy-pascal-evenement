package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HttpErrorHandler;
import com.aiconoa.trainings.services.LoginService;

@WebServlet("/EventLinkServlet")
public class EventLinkServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String USERNAME = "username";
    @Inject
    private EventService eventService;
    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Author author;
        HttpSession httpSession = request.getSession();

        try {
            if (httpSession.getAttribute(USERNAME) == null) {
                response.sendRedirect("LoginServlet");
            }
            String sessionUsername = httpSession.getAttribute(USERNAME).toString();
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!eventService.checkIfEventCorrespondToAuthor(idEvent, sessionUsername, response)) {
                return;
            }
            // recherche de l'autheur
            author = loginService.selectIdAuthorByUsername(sessionUsername);
            // recherche de l'evenement par l'idEvent
            Event event = eventService.selectEventByIdEvent(idEvent);
            // recherche des membres ajoutés et statuts pour l'IdEvent
            List<Subscriber> subscribers = eventService.listSelectSubscribersByIdEvent(idEvent);

            // envoi des arg a la jsp
            request.setAttribute("event", event);
            request.setAttribute("subscriberList", subscribers);
            request.setAttribute("author", author);
            request.getRequestDispatcher("/WEB-INF/eventLink.jsp")
                   .forward(request, response);

        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (ServletException | IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "forward impossible");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            HttpSession httpSession = request.getSession();
            if (httpSession.getAttribute(USERNAME) == null) {
                response.sendRedirect("LoginServlet");
                return;
            }
            String sessionUsername = httpSession.getAttribute(USERNAME).toString();
            int idEvent = Integer.parseInt(request.getParameter("id"));
            if (!eventService.checkIfEventCorrespondToAuthor(idEvent, sessionUsername, response)) {
                return;
            }
            // recherche de l'evenement par l'idEvent
            Event event = eventService.selectEventByIdEvent(idEvent);

            String emailList = request.getParameter("email");
            if (emailList == null || emailList.trim().isEmpty()) {
                response.sendRedirect("EventLinkServlet?id=" + idEvent);
                return;
            }

            eventService.addSubscribersToEvent(event, emailList);
            response.sendRedirect("EventLinkServlet?id=" + idEvent);

        } catch (NumberFormatException e) {
            HttpErrorHandler.print404(response, e, "Le parametre de la requete http ne peut pas etre converti en Integer dans le doPost de EventLinkServlet");
        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "redirection impossible");
            return;
        }
    }
}
