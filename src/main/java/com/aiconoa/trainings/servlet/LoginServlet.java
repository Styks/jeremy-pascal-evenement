package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.mindrot.jbcrypt.BCrypt;

import com.aiconoa.trainings.services.LoginService;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger();
    @Inject
    private LoginService loginService;
    private static final String USERNAME = "username";
    private static final String FLASHMESSAGE = "flash.message";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // presenter un formulaire de connection
        try {
            request.getRequestDispatcher("/WEB-INF/login.jsp")
                    .forward(request, response);
        } catch (Exception e) {
            LOGGER.error("forward impossible", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter(USERNAME);
        HttpSession httpSession = request.getSession();
        try {
            if (httpSession.getAttribute(USERNAME) != username) {
                response.getWriter().append("username was set in the session \n");
                httpSession.setAttribute(USERNAME, username);
            }
            if (!loginService.checkIfAuthorExist(username)) {
                request.setAttribute(FLASHMESSAGE, "Username inconnu");
                response.sendRedirect("LoginServlet");
                return;
            }
            String password = loginService.getPasswordByUserName(username);
            if (!BCrypt.checkpw(request.getParameter("password"), password)) {
                request.setAttribute(FLASHMESSAGE, "Mauvais password");
                response.sendRedirect("LoginServlet");
                return;
            }
            request.setAttribute(FLASHMESSAGE, "Vous vous êtes connectés avec succès");
            response.sendRedirect("EventsListServlet");
        } catch (Exception e) {
            LOGGER.error("probleme avec la reponse", e);
        }
    }
}
