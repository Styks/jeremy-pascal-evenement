package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.StatusAcceptDecline;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HashCodeChecker;
import com.aiconoa.trainings.services.HttpErrorHandler;

@WebServlet("/EventConfirmServlet")
public class EventConfirmServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Inject
    private EventService eventService;
    @Inject
    private HashCodeChecker hashCodeChecker;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String hash = request.getParameter("token");
            if (!hashCodeChecker.checkHashCode(hash)) {
                HttpErrorHandler.print404(response, "le hashcode suivant n'existe pas dans la bdd : " + hash);
                return;
            }

            Subscriber subscriber = eventService.selectSubscriberByHashcode(hash);
            Event event = eventService.selectEventByIdEvent(subscriber.getIdEvent());
            User user = eventService.selectUserByIdUser(subscriber.getIdUser());

            request.setAttribute("event", event);
            request.setAttribute("email", user.getEmail());

            request.getRequestDispatcher("/WEB-INF/eventConfirm.jsp")
                   .forward(request, response);
        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (ServletException | IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "forward impossible");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String hash = request.getParameter("token");
            if (!hashCodeChecker.checkHashCode(hash)) {
                HttpErrorHandler.print404(response, "le hashcode suivant n'existe pas dans la bdd : " + hash);
                return;
            }

            int responseUser = StatusAcceptDecline.valueOf(request.getParameter("responseUser")).getStatus();

            eventService.updateSubscribersIsPresentWhereHashCode(hash, responseUser);
            response.sendRedirect("EventConfirmValidationServlet?token=" + hash);

        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
            return;
        } catch (IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "redirection impossible");
            return;
        }
    }
}
