package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.EventServiceException;
import com.aiconoa.trainings.services.HttpErrorHandler;
import com.aiconoa.trainings.services.LoginService;
import com.aiconoa.trainings.services.Tools;

@WebServlet("/EventCreationServlet")
@MultipartConfig
public class EventCreationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Inject
    private EventService eventService;
    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession httpSession = request.getSession();
            if (httpSession.getAttribute("username") == null) {
                response.sendRedirect("LoginServlet");
                return;
            }
            String sessionUsername = httpSession.getAttribute("username").toString();
            Author author = loginService.selectIdAuthorByUsername(sessionUsername);
            if (request.getParameter("id") != null) { // Mode edition
                int idEvent = Integer.parseInt(request.getParameter("id"));
                if (!eventService.checkIfEventCorrespondToAuthor(idEvent, sessionUsername, response)) {
                    return;
                }
                Event event = eventService.selectEventByIdEvent(idEvent);
                request.setAttribute("event", event);
            }
            request.setAttribute("author", author);
            request.getRequestDispatcher("/WEB-INF/eventCreation.jsp")
                   .forward(request, response);
        } catch (NumberFormatException e) {
            HttpErrorHandler.print404(response, e, "Le parametre de la requete http ne peut pas etre converti en Integer dans le doGet de EventCreationServlet");
            return;
        } catch (EventServiceException eventServiceException) {
            HttpErrorHandler.print500(response, eventServiceException);
            return;
        } catch (ServletException | IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "forward impossible");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String idEventString = request.getParameter("id");
        try {
            HttpSession httpSession = request.getSession();
            if (httpSession.getAttribute("username") == null) {
                response.sendRedirect("LoginServlet");
                return;
            }
            String sessionUsername = httpSession.getAttribute("username").toString();
            String fileName = Tools.uploadMyFile(request);
            Author author = loginService.selectIdAuthorByUsername(sessionUsername);
            int idEvent;
            if (idEventString != null) { // update
                idEvent = Integer.parseInt(idEventString);
                if (!eventService.checkIfEventCorrespondToAuthor(idEvent, sessionUsername, response)) {
                    return;
                }
                eventService.updateEvent(idEvent, title, description, fileName);
                request.setAttribute("flash.message", "Vous avez édité un évènement avec succès");
            } else { // creation
                idEvent = eventService.insertEvent(title, description, fileName, author.getIdAuthor());
                request.setAttribute("flash.message", "Vous avez créé un évènement avec succès");
            }
            request.setAttribute("author", author);
            response.sendRedirect("EventLinkServlet?id=" + idEvent);

        } catch (NumberFormatException e) {
            HttpErrorHandler.print404(response, e, "Le parametre de la requete http ne peut pas etre converti en Integer dans le doPost de EventCreationServlet");
            return;
        } catch (IOException | IllegalStateException e) {
            HttpErrorHandler.print500(response, e, "redirection impossible");
        } catch (EventServiceException e) {
            HttpErrorHandler.print500(response, e);
        }
    }
}
