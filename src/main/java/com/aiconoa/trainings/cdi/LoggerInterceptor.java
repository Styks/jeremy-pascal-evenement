package com.aiconoa.trainings.cdi;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.aiconoa.training.annotations.Logged;

@Logged
@Interceptor
public class LoggerInterceptor {

    @AroundInvoke
    public Object aroundInvocation(InvocationContext invocationContext) throws Exception {
        return invocationContext.proceed();

    }
}