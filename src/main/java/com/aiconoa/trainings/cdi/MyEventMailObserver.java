package com.aiconoa.trainings.cdi;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import com.aiconoa.trainings.services.EventServiceException;

import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;


@ApplicationScoped
public class MyEventMailObserver {
    private static final Logger LOGGER = Logger.getLogger(MyEventMailObserver.class.getName());
    private static final  String ORIGIN_WEB_SITE = "http://localhost:8080/event";
    @Resource(lookup = "java:jboss/mail/Gmail")
    private Session session;
    @Inject
    private ServletContext context;

    public void listenToMyEvent(@Observes MyEventMail myEventMail) {
        LOGGER.info(String.format("received event %s", myEventMail));

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("pascaljeremym2i@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(myEventMail.getEmail()));
            message.setSubject("Invitation");

            // Freemarker configuration object
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);

            String imageLink = "<img src=\"" + ORIGIN_WEB_SITE + "/MailReadServlet?msg_id=" + myEventMail.getHash()
                    + "\"\" alt=\"\" width=\"\"0\"\" height=\"\"0\"\"style=\"\"width: 0px; height: 0px; border:0px;\"\" />";
            String link = ORIGIN_WEB_SITE + "/eventConfirm.xhtml?token=" + myEventMail.getHash();

            WebappTemplateLoader templateLoader = new WebappTemplateLoader(context, "WEB-INF/templates");

            templateLoader.setURLConnectionUsesCaches(false);
            templateLoader.setAttemptFileAccess(false);
            cfg.setTemplateLoader(templateLoader);

            cfg.setDefaultEncoding("UTF-8");

            // Build the data-model
            Map<String, Object> root = new HashMap<>();

            // Put datas into the root
            root.put("link", link);
            root.put("imageLink", imageLink);
            root.put("title", myEventMail.getTitle());

            Template temp = cfg.getTemplate("emailTemplate.ftl");
            Writer sw = new StringWriter();
            temp.process(root, sw);
            message.setContent(sw.toString(), "text/html; charset=utf-8");
            Transport.send(message);

        } catch (Exception e) {
            LOGGER.info("Issue with Freemarker template location within the class SendMailBySite");
            throw new EventServiceException("Error - Mail not send", e);
        }
    }
}