package com.aiconoa.trainings.entity;

public enum StatusAcceptDicline {
    ACCEPT(1, "Je viens"), DECLINE(2, "Je viens pas");

    private int Status;
    private String StatusInJSP;

    private StatusAcceptDicline(int status, String statusInJSP) {
        setStatus(status);
        setStatusInJSP(statusInJSP);
    }

    public String getStatusInJSP() {
        return StatusInJSP;
    }

    public void setStatusInJSP(String statusInJSP) {
        StatusInJSP = statusInJSP;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

public int getStatus(String value){
    return StatusAcceptDicline.valueOf(StatusInJSP).Status;
}

}
