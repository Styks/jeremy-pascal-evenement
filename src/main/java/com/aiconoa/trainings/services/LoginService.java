package com.aiconoa.trainings.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.aiconoa.trainings.entity.Author;

@Stateless
public class LoginService {

    @PersistenceContext(unitName = "event")
    private EntityManager em;
    private static final String USERNAME = "username";

    public LoginService() {
        super();
    }

    public boolean checkIfAuthorExist(String username) {
        String jpql = "SELECT COUNT(a) FROM Author a WHERE a.username=:username";
        TypedQuery<Long> query = em.createQuery(jpql, Long.class);
        query.setParameter(USERNAME, username);
        return query.getSingleResult() == 1L;
    }

    public Author selectIdAuthorByUsername(String username) {
        try {
            String jpql = "SELECT a FROM Author a WHERE a.username = :username";
            TypedQuery<Author> query = em.createQuery(jpql, Author.class);
            query.setParameter(USERNAME, username);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void insertAuthor(String username, String password) {
        Author a = new Author();
        a.setUsername(username);
        a.setPassword(password);
        em.persist(a);
    }

    public void insertAuthor(Author author) {
        em.persist(author);
    }

    public String getPasswordByUserName(String username) {
        String jpql = "SELECT a.password FROM Author a WHERE a.username = :username";
        TypedQuery<String> query = em.createQuery(jpql, String.class);
        query.setParameter(USERNAME, username);
        return query.getSingleResult();

    }

    public void insertToken(String username, String token) {
        Author a = selectIdAuthorByUsername(username);
        if (a != null) {
            a.setToken(token);
        }
    }

    public boolean checkIfTokenExist(String token) {
        String jpql = "SELECT COUNT(a) FROM Author a WHERE a.token=:token";
        TypedQuery<Long> query = em.createQuery(jpql, Long.class);
        query.setParameter("token", token);
        return query.getSingleResult() == 1L;
    }
}
