package com.aiconoa.trainings.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.aiconoa.trainings.filters.CORSFilter;

@ApplicationPath("/api")
public class RestApplication extends Application{

    @Override
    public Set<Class<?>> getClasses(){
        Set<Class<?>> resources = new HashSet<>();
        resources.add(EventResource.class);
        resources.add(EventResourceNotSecured.class);
        resources.add(AuthentificationFilter.class);
        resources.add(AuthentificationEndpoint.class);
        resources.add(CORSFilter.class);
        return resources;
    }
}
