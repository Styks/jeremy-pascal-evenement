package com.aiconoa.trainings.jaxrs;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;

import com.aiconoa.trainings.services.Hasher;
import com.aiconoa.trainings.services.LoginService;

@Path("/event/authentification")
public class AuthentificationEndpoint {
    @Inject
    private LoginService loginService;

    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response authenticateUser(@FormParam("username") String username, 
                                     @FormParam("password") String password) {

        try {

            // Authenticate the user using the credentials provided
            authenticate(username, password);

            // Issue a token for the user
            String token = issueToken(username);

            // Return the token on the response
            return Response.ok(token).build();

        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }      
    }

    private void authenticate(String username, String password) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        
        String passwordDB = loginService.getPasswordByUserName(username);

        if (!BCrypt.checkpw(password, passwordDB)) {
            throw new Exception();
        }

    }

    private String issueToken(String username) {
        String token=Hasher.sha1ToHex(username);
        loginService.insertToken(username,token);
         return token;
    }
}
