package com.aiconoa.trainings.jaxrs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.mindrot.jbcrypt.BCrypt;

import com.aiconoa.training.annotations.Secured;
import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.EventStatData;
import com.aiconoa.trainings.entity.Subscriber;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.services.EventService;
import com.aiconoa.trainings.services.LoginService;

@Path("/event")
public class EventResource {
    private static final int ACCEPT = 1;
    private static final int DECLINE = 2;
    private static final int PRESENT = 1;
    private static final int NOTPRESENT = 2;

    @Inject
    private EventService eventService;
    @Inject
    private LoginService loginService;

    @POST
    @Path("/register")
    public void postAuthor(Author author) {
        if (!loginService.checkIfAuthorExist(author.getUsername())) {
            String passwordDB = BCrypt.hashpw(author.getPassword(), BCrypt.gensalt(12));
            author.setPassword(passwordDB);
            loginService.insertAuthor(author);
        }
    }

    @POST
    @Secured
    @Path("/creation")
    public void postEvent(Event event, Author author) {
        eventService.insertEvent(event.getTitle(), event.getDescription(), event.getFileName(), author.getIdAuthor());
    }

    @GET
    @Secured
    @Path("/edition/{id}")
    public Event getEvent(@PathParam("id") int id) {
        return eventService.selectEventByIdEvent(id);
    }

    @PUT
    @Secured
    @Path("/edition/{id}")
    public void putEvent(Event event, @PathParam("id") int id) {
        eventService.updateEvent(id, event.getTitle(), event.getDescription(), event.getFileName());
    }

    @GET
    @Secured
    @Path("/link/{id}")
    public Event getEventById(@PathParam("id") int id) {
        return eventService.selectEventByIdEvent(id);
    }

    @POST
    @Secured
    @Path("/link/{id}")
    public void postSubcribers(@PathParam("id") int id, String emailList) {
        Event event = eventService.selectEventByIdEvent(id);
        eventService.addSubscribersToEvent(event, emailList);
    }

    @GET
    @Secured
    @Path("/list")
    public List<Event> getListEvent(@HeaderParam("Authorization") String tokenHeader) {
        String token = tokenHeader.substring("Bearer".length()).trim();
        return eventService.listSelectEventColumnsByToken(token);
    }
    
    
    @GET   
    @Path("/listByUser/{email}")
    public List<Subscriber> getListEventByUser(@PathParam("email") String email) {
        User user = new User();
        user.setEmail(email);
        return eventService.listSelectSubscriberByUser(user);
    }

    @GET
    @Path("/confirm/{hash}")
    public Event getConfirm(@PathParam("hash") String hash) {
        Subscriber subscriber = eventService.selectSubscriberByHashcode(hash);
        return eventService.selectEventByIdEvent(subscriber.getIdEvent());
    }

    @PUT
    @Path("/confirm/{hash}")
    public void putConfirm(@PathParam("hash") String hash, int choice) {
        eventService.updateSubscribersIsPresentWhereHashCode(hash, choice);
    }

    @GET
    @Path("/confirmValidation/{hash}")
    public Subscriber getConfirmValidation(@PathParam("hash") String hash) {
        return eventService.selectSubscriberByHashcode(hash);
    }

    @GET
    @Secured
    @Path("/stats/{id}")
    public EventStatData getConfirmValidation(@PathParam("id") int idEvent) {
        EventStatData eventStatData = new EventStatData();

        // recherche de l'evenement
        Event event = eventService.selectEventByIdEvent(idEvent);

        eventStatData.setIdEvent(idEvent);
        eventStatData.setTitle(event.getTitle());

        // recherche des membres ajoutés et statuts
        List<Subscriber> subscriberList = eventService.listSelectSubscribersByIdEvent(idEvent);
        eventStatData.setNbTotalInvitation(subscriberList.size());

        // recherche du nombre de participants ACCEPT
        eventStatData.setNbIsPresent(eventService.countNumberPresentOrNot(idEvent, ACCEPT));

        // recherche du nombre de participants DECLINE
        eventStatData.setNbIsNotPresent(eventService.countNumberPresentOrNot(idEvent, DECLINE));

        // recherche du nombre de mails lus
        eventStatData.setNbIsRead(eventService.countNumberMailRead(idEvent));

        // recherche du nombre de presents apres ouverture du mail
        eventStatData.setNbIsPresentAfterReading(
                eventService.countNbIsPresentOrNotAfterReading(idEvent, PRESENT));

        // recherche du nombre d'absents apres ouverture du mail
        eventStatData.setNbIsAbsentAfterReading(
                eventService.countNbIsPresentOrNotAfterReading(idEvent, NOTPRESENT));

        return eventStatData;
    }

}
