<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="css/bandeau.css" rel="stylesheet" type="text/css">
<ul class="bandeau">
  <li> <a> ${author.username} </a> </li>
  <li> <a style="color:green"> ${message} </a> </li>
  <li style="float:right"> <a class="active" href="LogoutServlet">Logout</a> </li>
</ul>
  <br>  <br>
