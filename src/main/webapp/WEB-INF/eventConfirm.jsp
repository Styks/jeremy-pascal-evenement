<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmation inscription</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">

<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="wrapper">
		<h1>Confirmation d'ajout à l'événement</h1>
		<br>
		<h1>${event.title}</h1>
		<br>
		<p>${event.description}</p>
		<br>
		<p>${email}</p>
		<br>
		<form class="form" method="post">
			<input type="submit" class="submit" value="ACCEPT" name="responseUser">
			<input type="submit" class="submit" value="DECLINE" name="responseUser">
		</form>
	</div>
	<p class="optimize">Optimized for Javengers!</p>
</body>
</html>