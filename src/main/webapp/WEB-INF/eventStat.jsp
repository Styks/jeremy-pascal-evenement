<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Statistiques de l'evenement</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
<script type="text/javascript"
	src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart1);
      google.charts.setOnLoadCallback(drawChart2);
      google.charts.setOnLoadCallback(drawChart3);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart1() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([['Présent', ${eventStatData.nbIsPresent}],['Non confirmé', ${eventStatData.nbNotConfirmed}],['Ne viens pas', ${eventStatData.nbIsNotPresent}]]);

        // Set chart options
        var options = {is3D: true,
        				'width':410,
                             'height':280,
                             'chartArea': {'width': '100%', 'height': '80%'},
                             backgroundColor: { fill:'transparent' },
                             colors: ['#37E54C', '#0085E7','red'],
                       legend: { position: 'bottom', alignment: 'center' ,textStyle: {color: 'white', fontSize: 14}}};

        // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.PieChart(document.getElementById('drawChart1'));

        chart.draw(data, options);
      }
      
      function drawChart2() {

          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Topping');
          data.addColumn('number', 'Slices');
          data.addRows([['Mails lus', ${eventStatData.nbIsRead}],['Mails non lus', ${eventStatData.nbIsNotRead}]]);

          // Set chart options
          var options = {is3D: true,
          				'width':410,
                               'height':280,
                               'chartArea': {'width': '100%', 'height': '80%'},
                               backgroundColor: { fill:'transparent' },
                               colors: ['#37E54C', '#0085E7'],
                         legend: { position: 'bottom', alignment: 'center' ,textStyle: {color: 'white', fontSize: 14}}};

          // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('drawChart2'));

          chart.draw(data, options);
        }
      
      function drawChart2() {

          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Topping');
          data.addColumn('number', 'Slices');
          data.addRows([['Mails lus', ${eventStatData.nbIsRead}],['Mails non lus', ${eventStatData.nbIsNotRead}]]);

          // Set chart options
          var options = {is3D: true,
          				'width':410,
                               'height':280,
                               'chartArea': {'width': '100%', 'height': '80%'},
                               backgroundColor: { fill:'transparent' },
                               colors: ['#37E54C', '#0085E7'],
                         legend: { position: 'bottom', alignment: 'center' ,textStyle: {color: 'white', fontSize: 14}}};

          // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('drawChart2'));

          chart.draw(data, options);
        }
      
      function drawChart3() {

          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Topping');
          data.addColumn('number', 'Slices');
          data.addRows([['Present après ouverture du mail', ${eventStatData.nbIsPresentAfterReading}],['Absent après ouverture du mail', ${eventStatData.nbIsAbsentAfterReading}],['Non confirmé après ouverture du mail', ${eventStatData.nbIsNotConfirmedAfterReading}]]);

          // Set chart options
          var options = {is3D: true,
          				'width':410,
                               'height':280,
                               'chartArea': {'width': '100%', 'height': '80%'},
                               backgroundColor: { fill:'transparent' },
                               colors: ['#37E54C', 'red', '#0085E7'],
                         legend: { position: 'bottom', alignment: 'center' ,textStyle: {color: 'white', fontSize: 14}}};

          // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('drawChart3'));

          chart.draw(data, options);
        }
    </script>
</head>
<body>
<jsp:include page="bandeau.jsp"/>
	<nav>
		<ul class="menu">
			<li><a href="EventCreationServlet?id=${eventStatData.idEvent}">Evenement</a></li>
			<li><a href="EventLinkServlet?id=${eventStatData.idEvent}">Statut</a></li>
			<li><a href="EventStatServlet?id=${eventStatData.idEvent}">Statistiques</a></li>
		</ul>
	</nav>
	<br>
	<br>
	<div class="wrapper">
		<h1>Statistiques de l'evenement</h1>
		<h1>${title}</h1>
		<br> <br>
		<div class="texte_white">Membres invités</div>
		<div class="emailList">
			<table>
				<tr>
					<td>Email</td>
					<td>Statut</td>
					<td>Date d'envoi</td>
				</tr>
				<c:forEach var="subscriber" items="${subscriberList}"
					varStatus="status">
					<tr>
						<td><c:out value="${subscriber.email}" /></td>
						<td><div align="center">
								<img alt="${subscriber.isPresentString}"
									src="${subscriber.iconeLink}" width="17" height="17" />
							</div></td>
						<td><c:out value="${subscriber.sendingDateString}" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<br>
		<!--Div that will hold the pie chart-->
		<div class="emailList">
			<div id="drawChart1"></div>
		</div>
		<div class="emailList">
			<div id="drawChart2"></div>
		</div>
		<div class="emailList">
			<div id="drawChart3"></div>
		</div>
	</div>
			<p class="optimize">Optimized for Javengers! - Username : ${author.username}</p>
</body>
</html>