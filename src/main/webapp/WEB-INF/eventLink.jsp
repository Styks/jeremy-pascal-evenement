<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Evenement crée</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
<jsp:include page="bandeau.jsp"/>
	<nav>
		<ul class="menu">
			<li><a href="EventCreationServlet?id=${event.idEvent}">Evenement</a></li>
			<li><a href="EventLinkServlet?id=${event.idEvent}">Statut</a></li>
			<li><a href="EventStatServlet?id=${event.idEvent}">Statistiques</a></li>
		</ul>
	</nav>
	<br>
	<br>
	<div class="wrapper">
		<h1>Inviter des amis à l'événement :</h1>
		<h1>${event.title}</h1>
		<br> <br>
		<img id="image" alt="" src="FilesServlet/${event.fileName}" >
		<form class="form" method="post">
			<input id="email" type="text" name="email"
				placeholder="Entrer l'email de la personne à inviter" /> <input
				type="submit" class="submit" value="Valider!"> <br> <br>
			<br>
			<div class="texte_white">Membres invités</div>
			<div class="emailList">
				<table>
					<tr>
						<td>Email</td>
						<td>Statut</td>
						<td>Date d'envoi</td>
					</tr>
					<c:forEach var="subscriber" items="${subscriberList}"
						varStatus="status">
						<tr>
							<td><c:out value="${subscriber.email}" /></td>
							<td><div align="center">
									<img alt="${subscriber.isPresentString}"
										src="${subscriber.iconeLink}" width="17" height="17" />
								</div></td>
							<td><c:out value="${subscriber.sendingDateString}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</form>
	</div>
			<p class="optimize">Optimized for Javengers! - Username : ${author.username}</p>
</body>
</html>