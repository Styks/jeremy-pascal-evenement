<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List des evenements</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
<jsp:include page="bandeau.jsp"/>
<div class="wrapper">
<br>
	<form class="form" method="post">
		<input type="submit" class="submit" value="nouvel evenement"
			name="newEvent"> <br> <br>
		<div class="texte_white">Liste des evenements</div>
		<div class="emailList">
			<table>
				<c:forEach var="event" items="${events}">
					<tr>
						<td><a href="${event.linkToEventLinkServlet}"><c:out value="${event.title}" /></a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
</div>
</body>
</html>