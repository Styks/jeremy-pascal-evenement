<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="wrapper">
		<h1>Connexion</h1>
		<form class="form" method="post">
			<input id="username" type="text" name="username" placeholder="Entrer un nom d'utilisateur" />
			<input id="password" type="password" name="password" placeholder="Entrer votre mot de passe" />
			<input type="submit" class="submit" value="Se connecter!">
		</form>
	</div>
	<p class="optimize">Optimized for Javengers!</p>
</body>
</html>