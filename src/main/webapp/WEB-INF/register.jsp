<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="wrapper">
		<h1>Création de compte</h1>
		
<c:if test="${not empty ErrorPassword}">
Erreur dans la vérification du mot de passe
</c:if>
		<form class="form" method="post">
			<input id="username" type="text" name="username" placeholder="Entrer un nom d'utilisateur" />
			<input id="password" type="password" name="password1" placeholder="Entrer votre mot de passe" />
			<input id="password" type="password" name="password2" placeholder="Vérifier votre mot de passe" />
			<input type="submit" class="submit" value="Créer mon compte!">
		</form>
	</div>
	<p class="optimize">Optimized for Javengers!</p>
</body>
</html>