<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Creation d'evenements</title>
<link href="css/eventCreation.css" rel="stylesheet" type="text/css">
<link href="css/bandeau.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
</head>
<body>
<jsp:include page="bandeau.jsp"/>
	<c:choose>
		<c:when test="${empty event.idEvent}">
			<div class="wrapper">
				<h1>Creation d'évènements</h1>
				<p>Inserer un titre et une descprition svp</p>
				<form class="form" method="post" enctype="multipart/form-data">
					<input id="title" type="text" name="title"
						placeholder="Entrer un titre" />
					<textarea rows="4" name="description"
						placeholder="Entrer une description..."></textarea>
					<br>
					<input type="file" name="file" accept=".jpg,.png,.gif"/>
					<br>
					<input type="submit" class="submit" value="Créer!">
				</form>
			</div>
		</c:when>
		<c:otherwise>
			<nav>
				<ul class="menu">
					<li><a href="eventCreation.xhtml?id=${event.idEvent}">Evenement</a></li>
					<li><a href="eventLink.xhtml?id=${event.idEvent}">Statut</a></li>
					<li><a href="eventStat.xhtml?id=${event.idEvent}">Statistiques</a></li>
				</ul>
			</nav>
			<br>
			<br>
			<div class="wrapper">
				<h1>Edition d'évènements</h1>
				<p>Inserer un titre et une descprition svp</p>
				<form class="form" method="post" enctype="multipart/form-data">
					<input id="title" type="text" name="title" value="${event.title}" />
					<textarea rows="4" name="description">${event.description}</textarea>
					<br>
					<input type="file" name="file"/>
					<br>
					<input type="submit" class="submit" value="Editer!">
				</form>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>